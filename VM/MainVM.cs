﻿using AvitoParse.Model;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows;

namespace AvitoParse.VM
{
    public class MainVM : BindableBase
    {
        readonly Parse parser = new Parse();
        readonly Graphs graphs = new Graphs();

        public DelegateCommand StartParse { get; }
        public DelegateCommand Chart { get; }
        public DelegateCommand OpenDir { get; }
        public MainVM()
        {
            parser.PropertyChanged += (s, e) => RaisePropertyChanged(e.PropertyName);
            graphs.PropertyChanged += (s, e) => RaisePropertyChanged(e.PropertyName);

            StartParse = new DelegateCommand(() =>
            {
                try
                {
                    parser.Search(query);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });

            OpenDir = new DelegateCommand(() => 
            {
                new OpenDirectory().OpenDir();
            });

            Chart = new DelegateCommand(() =>
            {
                graphs.GetListRequests();
                //добавить заполнение истории поиска
            });
        }

        public ReadOnlyObservableCollection<Ad> AdList => parser.Ads;
        public ReadOnlyObservableCollection<string> Requests => graphs.ListRequests;

        private string query;
        public string Query
        {
            get
            {
                return query;
            }
            set
            {
                query = value;
                RaisePropertyChanged("Query");
            }
        }
    }
}
