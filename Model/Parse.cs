﻿using AvitoParse.Model.DB;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace AvitoParse.Model
{
    class Parse : BindableBase
    {
        readonly  ObservableCollection<Ad> ads = new ObservableCollection<Ad>();
        public readonly ReadOnlyObservableCollection<Ad> Ads;

        readonly SynchronizationContext synchronizationContext;

        public Parse()
        {
            Ads = new ReadOnlyObservableCollection<Ad>(ads);
            synchronizationContext = SynchronizationContext.Current ?? new SynchronizationContext();
        }

        void UpdateUI(object state)
        {
            ads.Add(state as Ad);
            RaisePropertyChanged($"AdList");
        }


        /// <summary>
        /// Запустить поиск
        /// </summary>
        public void Search(string query)
        {
            new Task(() =>
            {
                SynchronizationContext.SetSynchronizationContext(SynchronizationContext.Current);


                string avitoUrl = "https://www.avito.ru/";

                string path = $@"{Directory.GetParent(@"Images\").FullName}";
                path = !Directory.Exists(path) ? Directory.CreateDirectory(path).FullName : path;

                SQLWorker worker = new SQLWorker();

                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                var chromeOptions = new ChromeOptions();
                // Путь к профилю Chrome, в котором произведена аутентификация на Avito
                /*chromeOptions.AddArguments(@"user-data-dir=C:\Users\Lfey\AppData\Local\Google\Chrome\User Data");
                chromeOptions.AddArguments(@"profile-directory=Profile 2");*/
                chromeOptions.AddArguments(@"user-data-dir=C:\Users\Lfey2\AppData\Local\Google\Chrome\User Data");
                chromeOptions.AddArguments(@"profile-directory=Profile 1");
                //chromeOptions.AddArguments(@"headless");

                using (IWebDriver driver = new ChromeDriver(driverService, chromeOptions))
                {
                    driver.Navigate().GoToUrl($"{avitoUrl}");

                    var searchTextBox = driver.FindElement(By.XPath($"//input[@data-marker=\"search-form/suggest\"]"));

                    searchTextBox.Click();
                    searchTextBox.SendKeys($"{query}");

                    var searchSubmit = driver.FindElement(By.XPath($"//button[@data-marker=\"search-form/submit-button\"]"));
                    searchSubmit.Click();
                    var url = driver.Url;

                    // Нужно как-то получать регион, если категория не распознана
                    string region = driver.FindElement(By.XPath($"//div[@data-marker=\"search-form/region\"]//div")).Text;
                    new Requests(region, query).Insert();

                    for (int i = 1; i <= driver.FindElements(By.XPath("//*[@data-marker=\"catalog-serp\"]//*[@data-marker=\"item\"]")).Count; i++)
                    {
                        long selId = 0;
                        long adId = 0;
                        string sellersRate = "0";
                        int sellersRateCount = 0;
                        int price = 0;
                        int views = 0;
                        string sellersImage = "/Image/DefaultAvatar";
                        string adTitle = "н/д";
                        string category = "н/д";
                        string source = "н/д";
                        string adDescription = "н/д";
                        string geo = "н/д";
                        string sellersName = "н/д";
                        string sellersType = "н/д";
                        string adDate = "н/д";
                        string adImage = "н/д";
                        string selSource = "н/д";

                        adTitle = driver.FindElement(By.XPath($"//*[@data-marker=\"item\"][{i}]//a[@data-marker=\"item-title\"]")).Text;
                        price = Convert.ToInt32(Regex.Replace(driver.FindElement(By.XPath($"//*[@data-marker=\"item\"][{i}]//span[@data-marker=\"item-price\"]")).Text, @"[^\d]+", "")); // Цена не указана или бесплатно пофиксить
                                                                                                                                                                                         
                        category = driver.FindElement(By.XPath($"//ul[@data-marker=\"rubricator/list\"]//*[2]//*//*[2]")).Text;

                        source = driver.FindElement(By.LinkText($"{adTitle}")).GetAttribute("href");

                        driver.Navigate().GoToUrl($"{source}");

                        adDescription = driver.FindElement(By.XPath("//*[@data-marker=\"item-view/item-description\"]")).Text.Replace(Environment.NewLine, " ").Replace("\"", "");
                        geo = driver.FindElement(By.XPath("//*[@itemprop=\"address\"]/span")).Text;
                        sellersName = driver.FindElement(By.XPath("//*[@data-marker=\"seller-info/name\"]")).Text;
                        sellersType = driver.FindElement(By.XPath("//*[@data-marker=\"seller-info/label\"]")).Text;
                        views = Convert.ToInt32(Regex.Replace(driver.FindElement(By.XPath("//*[@data-marker=\"item-view/total-views\"]")).Text, @"[^\d]+", ""));
                        adDate = driver.FindElement(By.XPath("//*[@data-marker=\"item-view/item-date\"]")).Text.Replace("·", "").Trim();
                        adImage = driver.FindElement(By.XPath("//*[@data-marker=\"image-frame/image-wrapper\"]//img")).GetAttribute("src");
                        adId = Convert.ToInt64(Regex.Replace(driver.FindElement(By.XPath($"//*[@data-marker=\"item-view/item-id\"]")).Text, @"[^\d]+", ""));

                        DownloadFileAsync(adImage, $@"{path}\Ad{adId}.png");

                        selSource = driver.FindElement(By.XPath("//*[@data-marker=\"seller-info/avatar\"]//a")).GetAttribute("href"); // Траблы с парсингом компаний, проще скипать + траблы с парсингом с headless и узкими мониторами - скрывается информация о продавце
                        selId = Convert.ToInt64(new Regex(@"iid=(\d*)").Match(selSource).Value.Replace("iid=", ""));

                        driver.Navigate().GoToUrl($"{selSource}");

                        try
                        {
                            sellersImage = driver.FindElement(By.XPath("//img[@data-marker=\"avatar/image\"]")).GetAttribute("src"); // Не у всех (у компаний) парсятся аватарки, сделать дефолтную аватарку
                            DownloadFileAsync(sellersImage, $@"{path}\Avatar{selId}.png"); // сделать проверку на существование в папке, чтобы не нагружать сеть
                            sellersRate = driver.FindElement(By.XPath($"//span[@data-marker=\"profile/score\"]")).Text.Replace(",", ".");
                            sellersRateCount = Convert.ToInt32(Regex.Replace(driver.FindElement(By.XPath($"//a[@data-marker=\"profile/summary\"]")).Text, @"[^\d]+", ""));
                        }
                        catch { }

                        driver.Navigate().GoToUrl($"{url}");

                        new SQLWorker().InsertDataInDB(new Ads { SelId = selId, Title = adTitle, AdDescription = adDescription, Price = price, Address = geo, Views = views, AdDate = adDate, AdImage = $@"{path}\Ad{adId}.png", Source = source, Category = category },
                            new Sellers { SelId = selId, SellersRateCount = sellersRateCount, SellersRate = sellersRate, SellersType = sellersType, SellersName = sellersName, SellersImage = $@"{path}\Avatar{selId}.png" },
                            new Categories { Category = category });

                        synchronizationContext.Send(UpdateUI, new Ad { Image = adImage, Description = adDescription, Geo = geo, Price = price.ToString(), Seller = sellersName, SellersRate = sellersRate, SellersType = sellersType, Title = adTitle });
                    }
                    new Characteristics_ORM().Calculate();
                    driver.Quit();
                }
            }).Start();
        }

        /// <summary>
        /// Скачивание файла по url
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        async void DownloadFileAsync(string url, string fileName)
        {
            byte[] data;

            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url))
            using (HttpContent content = response.Content)
            {
                data = await content.ReadAsByteArrayAsync();
                using (FileStream file = File.Create($"{fileName}"))
                {
                    file.Write(data, 0, data.Length);
                }
            }
        }
    }
}