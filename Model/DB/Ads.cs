﻿namespace AvitoParse.Model
{
    class Ads
    {
        public long SelId { get; set; }
        public string AdImage { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public string Address { get; set; }
        public string AdDescription { get; set; }
        public int Views { get; set; }
        public string AdDate { get; set; }
        public string Source { get; set; }
        public string Category { get; set; }
    }
}