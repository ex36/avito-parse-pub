﻿using AvitoParse.Model;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Windows;

namespace AvitoParse
{
    // Переделать всё в async
    /// <summary>
    /// Работа с БД
    /// </summary>
    class SQLWorker
    {
        /// <summary>
        /// Проверяет, есть ли поле, соответствующее sqlExpression. Возвращает 0, если нет полей, есть есть поле - возвращает его значение
        /// </summary>
        /// <param name="sqlExpression"></param>
        /// <returns></returns>
        public long SelectExecuteScalar(string sqlExpression)
        {
            string connectionString = Properties.Settings.Default.kdbConnectionString;
            long i = 0;
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                using (OleDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = sqlExpression;
                    long l = Convert.ToInt64(command.ExecuteScalar());
                    i = l != 0 ? l : 0;
                }
                connection.Close();
            }
            return i;
        }

        /// <summary>
        /// Выбрать все записи, имеющие тип int
        /// </summary>
        /// <param name="sqlExpression"></param>
        /// <returns></returns>
        public List<int> SelectAllInt(string sqlExpression)
        {
            string connectionString = Properties.Settings.Default.kdbConnectionString;
            List<int> i = new List<int>();
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                using (OleDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = sqlExpression;

                    using (OleDbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            if (!reader.IsDBNull(0))
                                i.Add(reader.GetInt32(0));
                        reader.Close();
                    }
                }
                connection.Close();
            }
            return i;
        }
        /// <summary>
        /// Выбрать все записи, имеющие тип string
        /// </summary>
        /// <param name="sqlExpression"></param>
        /// <returns></returns>
        public List<string> SelectAllStr(string sqlExpression)
        {
            string connectionString = Properties.Settings.Default.kdbConnectionString;
            List<string> i = new List<string>();
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                using (OleDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = sqlExpression;

                    using (OleDbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            if (!reader.IsDBNull(0))
                                i.Add(reader.GetString(0));
                        reader.Close();
                    }
                }
                connection.Close();
            }
            return i;
        }
        /// <summary>
        /// Выбрать все записи, имеющие тип DateTime
        /// </summary>
        /// <param name="sqlExpression"></param>
        /// <returns></returns>
        public List<DateTime> SelectAllDT(string sqlExpression)
        {
            string connectionString = Properties.Settings.Default.kdbConnectionString;
            List<DateTime> i = new List<DateTime>();
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                using (OleDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = sqlExpression;

                    using (OleDbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            if (!reader.IsDBNull(0))
                                i.Add(reader.GetDateTime(0));
                        reader.Close();
                    }
                }
                connection.Close();
            }
            return i;
        }


        /// <summary>
        /// Вставка данных в базу данных через SQL запрос
        /// </summary>
        public void InsertData(string sqlExpression)
        {
            string connectionString = Properties.Settings.Default.kdbConnectionString;

            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (OleDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = sqlExpression;
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();

                        command.Transaction = transaction;
                        transaction.Commit(); // https://learn.microsoft.com/ru-ru/dotnet/framework/data/adonet/local-transactions
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    MessageBox.Show($"{ex.Message} {sqlExpression}");
                    using (StreamWriter writer = new StreamWriter("catchlog.txt", true))
                    {
                        writer.WriteLine($"{DateTime.Now}\n{ex.Message} {sqlExpression}");
                        writer.Close();
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Вставка данных в базу данных
        /// </summary>
        public void InsertDataInDB(Ads ads, Sellers sellers, Categories categories)
        {
            string connectionString = Properties.Settings.Default.kdbConnectionString;

            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                //OleDbTransaction transaction = connection.BeginTransaction();
                using (OleDbCommand command = connection.CreateCommand())
                {
                    try
                    {
                        command.Transaction = connection.BeginTransaction();

                        if (SelectExecuteScalar($"SELECT sel.id From Sellers sel Where sel.id = {sellers.SelId}") == 0)
                        {
                            command.CommandText = $"INSERT INTO Sellers(id, sellers_RateCount, sellers_Rate, sellers_Type, sellers_Name, sellers_Image) " +
                                $"VALUES({sellers.SelId}, {sellers.SellersRateCount}, {sellers.SellersRate}, \"{sellers.SellersType}\", \"{sellers.SellersName}\", \"{sellers.SellersImage}\")";
                            command.ExecuteNonQuery();
                        }

                        if (!SelectExitOrNot($"SELECT COUNT(cat_name) From Categories Where cat_name = \"{categories.Category}\""))
                        {
                            command.CommandText = $"INSERT INTO Categories(cat_name) VALUES(\"{categories.Category}\")";
                            command.ExecuteNonQuery();
                        }

                        command.CommandText = $"INSERT INTO Ads(req_id, cat_id, sel_id, title, ad_description, price, address, views, ad_date, ad_image, source) " +
                            $"SELECT (SELECT LAST(id) FROM Requests), c.id, {ads.SelId}, \"{ads.Title}\", \"{ads.AdDescription}\", {ads.Price}, \"{ads.Address}\", {ads.Views}, \"{ads.AdDate}\", \"{ads.AdImage}\", \"{ads.Source}\" " +
                            $"FROM Categories c WHERE c.cat_name = \"{ads.Category}\"";
                        command.ExecuteNonQuery();

                        command.Transaction.Commit();
                        //transaction.Commit(); // https://learn.microsoft.com/ru-ru/dotnet/framework/data/adonet/local-transactions
                    }
                    catch (Exception ex)
                    {
                        command.Transaction.Rollback();

                        MessageBox.Show($"{ex.Message}");
                        using (StreamWriter writer = new StreamWriter("catchlog.txt", true))
                        {
                            writer.WriteLine($"{DateTime.Now}\n{ex.Message}");
                            writer.Close();
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Проверяет, есть ли поле, соответствующее sqlExpression. Возвращает true, если есть поле, false - отсутствует
        /// </summary>
        /// <param name="sqlExpression"></param>
        /// <returns></returns>
        public bool SelectExitOrNot(string sqlExpression)
        {
            string connectionString = Properties.Settings.Default.kdbConnectionString;
            bool b = true;
            int i = 1;
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                using (OleDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = sqlExpression;
                    using (OleDbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            if (!reader.IsDBNull(0) && reader.GetValue(0) is int)
                                i = reader.GetInt32(0);
                        reader.Close();
                    }
                }
                connection.Close();
            }
            b = i == 1;
            return b;
        }
    }
}
