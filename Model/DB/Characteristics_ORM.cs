﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoParse.Model.DB
{
    class Characteristics_ORM
    {
        double average = 0;
        double RMS = 0;
        int mode = 0;
        int min = 0;
        int max = 0;
        int median = 0;

        public void Calculate()
        {
            using (var con = new OleDbConnection(Properties.Settings.Default.kdbConnectionString))
            {
                con.Open();

                var ads = con.Query<Ads>("SELECT price FROM Ads Where req_id  = (SELECT LAST(req_id) FROM Ads)");
                List<int> prices = ads.Select(x => x.Price).ToList();
                if (prices.Count != 0)
                {
                    average = Math.Round(ads.Average(x => x.Price), 2);
                    median = GetMedian(prices); // Нахождение медианы
                    mode = GetMode(prices); // Нахождение моды
                    min = ads.Min(x => x.Price);
                    max = ads.Max(x => x.Price);
                    RMS = Math.Round(GetRMS(prices, average), 2); // Нахождение СКО
                }

                con.Query<Characteristics>($"INSERT INTO Characteristics(id, char_average, char_RMS, char_mode, char_median, char_min, char_max) " +
                $"SELECT LAST(req.id), {average.ToString().Replace(",", ".")}, {RMS.ToString().Replace(",", ".")}, {mode}, {median}, {min}, {max} FROM Requests req");
                con.Close();
            }
        }
        #region Методы для высчитывания моды, медианы и СКО
        private double GetRMS(List<int> prices, double average)
        {
            double RMS = 0;
            double i = 0;
            foreach (double price in prices)
            {
                i += Math.Pow(price - average, 2);
            }
            RMS = Math.Sqrt(i / (prices.Count - 1));
            return RMS;
        }

        private int GetMode(List<int> prices)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            foreach (int elem in prices)
                dict[elem] = dict.ContainsKey(elem) ? dict[elem]++ : dict[elem] = 1;

            int maxCount = 0;
            int mode = 0;
            foreach (int elem in dict.Keys)
            {
                if (dict[elem] > maxCount)
                {
                    maxCount = dict[elem];
                    mode = elem;
                }
            }

            return mode;
        }

        private int GetMedian(List<int> prices)
        {
            prices.Sort();
            return prices.Count / 2 == 0 ? prices[prices.Count / 2 - 1] : prices[prices.Count / 2];
        }
        #endregion
    }
}
