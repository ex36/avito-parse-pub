﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Documents;

namespace AvitoParse.Model
{
    class Characteristics
    {
        public double Char_average { get; set; }
        public double Char_RMS { get; set; }
        public double Char_mode { get; set; }
        public double Char_median { get; set; }
        public double Char_min { get; set; }
        public double Char_max { get; set; }
    }
}