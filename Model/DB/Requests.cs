﻿namespace AvitoParse.Model
{
    class Requests
    {
        string region;
        string query;
        public Requests(string region, string query)
        {
            this.region = region;
            this.query = query;
        }

        /// <summary>
        /// Вставка записи в БД
        /// </summary>
        public void Insert()
        {
            new SQLWorker().InsertData($"INSERT INTO Requests(region, query) VALUES(\"{region}\", \"{query}\")");
        }
    }
}
