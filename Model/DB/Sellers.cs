﻿namespace AvitoParse.Model
{
    class Sellers
    {
        public long SelId { get; set; }
        public int SellersRateCount { get; set; }
        public string SellersRate { get; set; }
        public string SellersType { get; set; }
        public string SellersName { get; set; }
        public string SellersImage { get; set; }
    }
}