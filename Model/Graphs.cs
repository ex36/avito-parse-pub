﻿using Dapper;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.OleDb;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AvitoParse.Model
{
    class Graphs : BindableBase
    {
        readonly ObservableCollection<string> listRequests = new ObservableCollection<string>();
        public readonly ReadOnlyObservableCollection<string> ListRequests;

        readonly SynchronizationContext synchronizationContext;
        public Graphs()
        {
            ListRequests = new ReadOnlyObservableCollection<string>(listRequests);
            synchronizationContext = SynchronizationContext.Current ?? new SynchronizationContext();
        }

        public void GetListRequests()
        {
            new Task(() => 
            {
                SynchronizationContext.SetSynchronizationContext(synchronizationContext);

                List<DateTime> list = new SQLWorker().SelectAllDT("SELECT time FROM Requests");
                foreach (DateTime date in list)
                    synchronizationContext.Send(UpdateUI, date);
            }).Start();
        }

        void UpdateUI(object state)
        {
            listRequests.Add(Convert.ToDateTime(state).ToString("G"));
            RaisePropertyChanged("Requests");
        }
    }
}
