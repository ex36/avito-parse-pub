﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoParse.Model
{
    public class OpenDirectory
    {
        public void OpenDir()
        {
            System.Diagnostics.Process.Start("explorer", $@"{Directory.GetCurrentDirectory()}\UserFolder");
        }
    }
}
