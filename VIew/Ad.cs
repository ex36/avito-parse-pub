﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoParse
{
    public class Ad
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Price { get; set; }
        public string Geo { get; set; }
        public string Description { get; set; }
        public string Seller { get; set; }
        public string SellersRate { get; set; }
        public string SellersType { get; set; }
        public string Source { get; set; }
    }
}
