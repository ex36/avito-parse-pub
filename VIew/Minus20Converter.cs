﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AvitoParse.View
{
    internal class Minus20Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => (string)parameter == "minusTwenty" ? $"{int.Parse(value.ToString()) - 20}" : value.ToString();

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}
