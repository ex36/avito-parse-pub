﻿using AvitoParse.Model;
using ScottPlot;
using System.Collections.Generic;
using System.Data.OleDb;
using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Application = System.Windows.Application;
using Dapper;
using System.Linq;

namespace AvitoParse.View
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Border_MouseLeftButtonUpExit(object sender, MouseButtonEventArgs e) => Application.Current.Shutdown();

        private void Border_MouseLeftButtonUpMax(object sender, MouseButtonEventArgs e) => WindowState = Application.Current.MainWindow.WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;

        private void Border_MouseLeftButtonUpMin(object sender, MouseButtonEventArgs e) => Application.Current.MainWindow.WindowState = WindowState.Minimized;
        private void SearchTabBtn_Click(object sender, RoutedEventArgs e)
        {
            TabControl.SelectedIndex = 0;
        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                SearchBtn.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }
        /*
        Many charting libraries use MVVM and data binding patterns to interact with plots.ScottPlot does not.
        This intentional decision allows ScottPlot to be more performant by giving the user raw access to array values used for plotting, 
        and also enabling users to have total control over when new frames are rendered(a potentially costly operation).
        Although MVVM pattern and data binding are often useful when designing interactive GUI applications,
        consider that virtually all ScottPlot functionality can be used to create static images from headless console applications where these patterns are less widely used.

        Поэтому нужно отказываться от ScottPlot
        */

        // Блокируется UI
        private void ChartBtn_Click(object sender, RoutedEventArgs e)
        {
            // https://scottplot.net/cookbook/4.1/
            Chart.Plot.Clear();

            TabControl.SelectedIndex = 1;

            string time = Cmb.Text;

            if (!String.IsNullOrEmpty(time))
            {
                List<int> ints = GetPrices("#" + time.Replace(".", "/") + "#");
                List<double> list = new List<double>();

                for (int i = 0; i < ints.Count; i++)
                {
                    list.Add(i);
                }

                Chart.Plot.Title("График");
                Chart.Plot.XLabel("Количество объявлений");
                Chart.Plot.YLabel("Цена товара");

                double[] dataX = list.ToArray();
                double[] dataY = Array.ConvertAll<int, double>(ints.ToArray(), Convert.ToDouble);
                string label = GetLabel("#" + time.Replace(".", "/") + "#");

                double AVG = GetAVG("#" + time.Replace(".", "/") + "#");
                double RMS = GetRMS("#" + time.Replace(".", "/") + "#");

                Chart.Plot.Style(figureBackground: Color.White,
                                 dataBackground: Color.White,
                                 grid: Color.FromArgb(37, 49, 109),
                                 tick: Color.FromArgb(37, 49, 109),
                                 axisLabel: Color.Gray,
                                 titleLabel: Color.LightGray);

                Chart.Plot.AddScatter(dataX, dataY, label: label, lineWidth: 2);
                Chart.Plot.AddHorizontalLine(AVG, label: "Среднее значение", width: 2);
                Chart.Plot.AddHorizontalLine(AVG + RMS, label: "Верхняя граница", width: 3, style: LineStyle.DashDotDot, color: Color.MediumPurple);
                Chart.Plot.AddHorizontalLine(AVG - RMS, label: "Нижняя граница", width: 3, style: LineStyle.DashDotDot, color: Color.MediumPurple);

                for (int i = 0; i < dataY.Length; i++)
                    if (dataY[i] < AVG + RMS && dataY[i] > AVG - RMS)
                        Chart.Plot.AddPoint(i, dataY[i], color: Color.Red).MarkerSize = 8;

                Chart.Plot.Legend();
                Chart.Refresh();
            }
        }
        // учесть выбросы в ценах и отбросить их
        #region Методы для вычисления всякого
        public List<int> GetPrices(string time)
        {
            List<int> nums;
            using (var con = new OleDbConnection(Properties.Settings.Default.kdbConnectionString))
            {
                con.Open();

                var ads = con.Query<Ads>($"SELECT price FROM Ads ad INNER JOIN Requests re on ad.req_id = re.id Where re.time = {time}");

                nums = ads.Select(x => x.Price).ToList();
                con.Close();
            }
            return nums;
        }
        public string GetLabel(string time) => new SQLWorker().SelectAllStr($"SELECT query FROM Requests Where time = {time}").First();
        public double GetRMS(string time)
        {
            double RMS;
            using (var con = new OleDbConnection(Properties.Settings.Default.kdbConnectionString))
            {
                con.Open();

                var characteristics = con.Query<Characteristics>($"SELECT ch.char_RMS FROM Characteristics ch INNER JOIN Requests re on ch.id = re.id Where re.time = {time}");

                RMS = characteristics.Select(x => x.Char_RMS).First();
                con.Close();
            }
            return RMS;
        }
        public double GetAVG(string time)
        {
            double AVG;
            using (var con = new OleDbConnection(Properties.Settings.Default.kdbConnectionString))
            {
                con.Open();

                var ads = con.Query<Ads>($"SELECT ch.char_average FROM Characteristics ch INNER JOIN Requests re on ch.id = re.id Where re.time = {time}");

                AVG = ads.Select(x => x.Price).Average();
                con.Close();
            }
            return AVG;
        }
        #endregion
    }
}
